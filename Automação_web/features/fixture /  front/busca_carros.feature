
#language:pt

Funcionalidade: Busca de veiculos no Site Webmotors 

Contexto: Busca de veiculos 

@pagina
Cenario: Acessando pagina de Resultados de Veiculos 

Dado que acesso a pagina "webmotors"
Quando clicar em "busca avançada"
Então devo ver a pagina de estoque de veiculos 


@busca
Cenario: Busca de um veiculo

Dado que acesso a pagina "estoque_carros"
Quando selecionar a Marca, Modelo e Versao do veiculo 
Então devo ver no resultado da pagina 

@loja
Cenario: Busca de estoque por revendedor 

Dado que acesso a pagina do revendedor "mazola_automoveis"
Quando este revendedor possuir carros disponiveis no estoque
Então devo ver uma lista de carros disponiveis e sua quantidade de estoque 


