Dado('que acesso a pagina {string}') do |webmotors|
    url = DATA[webmotors]    
    visit(url)
end
  
Quando('clicar em {string}') do |string|
    @busca.links_pagina_busca
end
  
Então('devo ver a pagina de estoque de veiculos') do
    @busca.verifica_texto_estoque
end

Quando('selecionar a Marca, Modelo e Versao do veiculo') do
    @busca.pesquisando_carro
end
  
Então('devo ver no resultado da pagina') do
    @busca.resultado_pagina
end

Dado('que acesso a pagina do revendedor {string}') do |revendedor|
    url = DATA[revendedor]    
    visit(url)
end
  
Quando('este revendedor possuir carros disponiveis no estoque') do
    @busca.validar_lista_carros_page
end
  
Então('devo ver uma lista de carros disponiveis e sua quantidade de estoque') do
    @busca.validar_estoque
end