class Busca 

    include RSpec::Matchers
    include Capybara::DSL

    def initialize
        @botao_aceite_cookies                = EL["botao_aceite_cookies"]
        @hover_comprar                       = EL["hover_comprar"]
        @link_compra_avancada                = EL["link_compra_avancada"]
        @verifica_texto_pagina_estoque       = EL['verifica_texto_pagina_estoque']
        @localizacao_cidade_estado           = EL['localizacao_cidade_estado']
        @selecionando_cidade                 = EL['selecionando_cidade']
        @imagem_busca_carro_honda            = EL['imagem_busca_carro_honda']
        @link_todos_modelos                  = EL['link_todos_modelos']
        @modelo_carro                        = EL['modelo_carro']
        @link_versao_carro                   = EL['link_versao_carro']
        @versao_carro                        = EL['versao_carro']
        @verifica_pagina_carro_escolhido     = EL['verifica_pagina_carro_escolhido']
        @bloco_carro                         = EL['bloco_carro']

    end

    def links_pagina_busca

        find(@botao_aceite_cookies).click
        first(@hover_comprar).hover
        find(@link_compra_avancada).click
        

    end

    def verifica_texto_estoque

        find(@verifica_texto_pagina_estoque,:visible => true)

    end

    def pesquisando_carro

        cidade = "São Paulo - SP"

        find(@localizacao_cidade_estado).set cidade
        find(@selecionando_cidade).click
        find(@imagem_busca_carro_honda).click
        find(@link_todos_modelos).click
        find(@modelo_carro).click
        find(@link_versao_carro).click
        find(@versao_carro).click
        sleep 5

       

    end

    def resultado_pagina
        find(@verifica_pagina_carro_escolhido,:visible =>true)
        
    end

    def validar_lista_carros_page

        assert_selector(@bloco_carro)
        
    end

    def validar_estoque
        quantidade_carros = all(@bloco_carro).count
        
        if quantidade_carros > 0 
            validador = true
        else 
            validador = false 
        end

        expect(validador).to be true
 
    end
end



        
