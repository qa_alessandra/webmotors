Before do
    
    @busca  = Busca.new
    current_window.maximize
    
end


After do
    shot_file = page.save_screenshot("log/screenshot.png")
    shot_b64 = Base64.encode64(File.open(shot_file, "rb").read)
    #embed(shot_b64, "image/png", "Screenshot") # Cucumber anexa o screenshot no report
end

at_exit do
    ReportBuilder.configure do |config|
        config.input_path = "data/report.json"
        config.report_path = "data/cucumber_web_report"
        config.report_types = [:html]
        config.color = "indigo"
        config.report_tabs = %w[Overview Features Scenarios Errors]
        config.report_title = "Automação de Testes - Report"
        config.compress_images = true
        config.additional_info = { "Projeto" => "Webmotors", "Canal/Ambiente" => "Produção", "QA" => "Alessandra Godoy" }
      end
    ReportBuilder.build_report
end